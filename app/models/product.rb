class Product < ApplicationRecord
    validates :nombre, presence: true
    validates :precio, presence: true
    validates :cantidad, presence: true
end
