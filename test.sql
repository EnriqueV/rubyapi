-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 30, 2020 at 04:59 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `ar_internal_metadata`
--

CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_internal_metadata`
--

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`) VALUES
('environment', 'development', '2020-05-28 23:49:01.013880', '2020-05-28 23:49:01.013880');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) NOT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `cantidad` varchar(255) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `img_url` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `nombre`, `description`, `cantidad`, `precio`, `img_url`, `created_at`, `updated_at`) VALUES
(4, '2334', 'Nike Jordan', NULL, '1', 99.99, 'https://i.pinimg.com/474x/e4/30/4f/e4304fa4a9aa31f3f977bcba79554b92.jpg', '2020-05-29 02:31:46.699814', '2020-05-29 02:31:46.699814'),
(5, '2334', 'Nike Air', NULL, '10', 89, 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto/i1-728eb69b-04be-4f07-8483-15f4829d4cb5/calzado-air-max-2090-3pVM46.jpg', '2020-05-29 02:33:33.591846', '2020-05-29 02:33:33.591846'),
(6, '2334', 'Nike Air Pro Max', 'nike air for runners', '5', 89, 'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto/i1-728eb69b-04be-4f07-8483-15f4829d4cb5/calzado-air-max-2090-3pVM46.jpg', '2020-05-29 02:39:32.208873', '2020-05-29 02:39:32.208873');

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20200528231856'),
('20200529013835');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password_digest` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `telefono`, `fecha_nacimiento`, `username`, `email`, `password_digest`, `created_at`, `updated_at`) VALUES
(1, 'eduardo', NULL, NULL, 'enriquev', 'kalienrique40@gmail.com', '$2a$12$L7B8zhRJiRNpNNwhnF1C.eGZJFMSmW7mRq.r6WVA8H4zf4A9VnVQK', '2020-05-29 00:04:55.956495', '2020-05-29 00:04:55.956495'),
(2, 'enrique gomez', '78785888', '02051993', 'enriqueG', 'kali@proyectosees.com', '$2a$12$f.9obcvEQCm9xWeOn.Pm8.GcIzqWNXD1k8uraFLC8Qh1/eWPDgqBa', '2020-05-29 02:24:14.950228', '2020-05-29 02:24:14.950228');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ar_internal_metadata`
--
ALTER TABLE `ar_internal_metadata`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schema_migrations`
--
ALTER TABLE `schema_migrations`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
