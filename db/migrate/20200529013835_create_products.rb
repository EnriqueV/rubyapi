class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.string :sku
      t.string :nombre
      t.string :description
      t.string :cantidad
      t.string :int
      t.float :precio
      t.string :img_url

      t.timestamps
    end
  end
end
