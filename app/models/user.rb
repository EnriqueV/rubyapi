class User < ApplicationRecord
    has_secure_password
    validates :email, presence: true, uniqueness: true
    validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
    validates :username, presence: true, uniqueness: true
    validates :telefono,
              length: { minimum: 8 },
              if: -> { new_record? || !telefono.nil? }
    validates :password,
              length: { minimum: 6 },
              if: -> { new_record? || !password.nil? }
  end