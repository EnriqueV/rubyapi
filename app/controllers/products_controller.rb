class ProductsController < ApplicationController
    before_action :authorize_request, except: :create
    before_action :find_user, except: %i[create index]
    # GET /products
    def index
      @products = Product.all
      render json: @products, status: :ok
    end
    # POST /products
    def create
        @product= Product.new(product_params)
        if @product.save
        render json: @product, status: :created
        else
        render json: { errors: @product.errors.full_messages },
                status: :unprocessable_entity
        end
    end
    def product_params
        params.permit(
          :sku, :nombre, :description, :cantidad, :precio, :img_url
        )
      end
end
