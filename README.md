# RubyApi

Simple ruby api for EV

## Installation

clone this [repo](https://gitlab.com/EnriqueV/rubyapi.git) https://gitlab.com/EnriqueV/rubyapi.git
```bash
cd rubyapi
```
## db import
- import file.sql 
- configure connection in confing/database.yml

## dependencies 

```
bundle install
```

```
rake db:create
```
```
rake db:migrate
```

## test project
```
rails server
```

## insomnia file
- Insomnia_2020-05-28.yaml
- Insomnia_Eleaniin.json

## test operations crud

- api endpoint [repo] (https://mysterious-reef-97460.herokuapp.com/products)

